# Rebalanced Diet

## The Summary

This data pack for Minecraft: Java Edition (and behavior pack for Bedrock) removes the redundant food types and rebalances the rest. It contains all the changes in [Porkchop Master Race](https://gitlab.com/ZebulanStanphill/porkchop-master-race), plus a whole lot more.

## The Story

Long ago, in the age of Beta 1.7, there were only a few food types, and each had its niche. Porkchops healed a lot, but required active hunting and cooking. Bread required growing wheat, but wheat was far more space efficient, easy to craft into bread, and was far easier to consistently farm, especially if you had bone meal, water, and pistons. Mushroom stew was also relatively storage efficient, and could be farmed slower, but more hands-free than wheat. Cakes were the most convoluted to create, but doubled as decoration. Golden apples were rare treats that acted as the best in-game food. The only "bad" foods were cookies and regular apples, which were either far too rare, or effectively non-existent in survival.

In the time since then, Mojang has both expanded the food palette and completely overhauled how food works. Unfortunately, many of these additions have added little to the game, and have served only to bloat the food system and unbalance it. Pigs are now useless compared to other farm animals. Various crops like potatoes and beetroots have added almost nothing to the game, yet now plague inventory management by competing for space with similar food items.

This pack attempts to relieve the problem by removing several foods that are particularly redundant. Notably, it restores pigs to their former status as the only land animal to drop meat, thus making them useful again.

## The Details

### Changes in both versions

#### Entity drops

Removed meat drops from chickens, cows, mooshrooms, rabbits and sheep. Pigs are now the only passive, non-aquatic mob that drops food, thus making them useful again.

To prevent the other passive mobs from sometimes dropping nothing, their loot tables have been adjusted:

Raised minimum feather drop count for chickens from 0 to 1.
Raised minimum leather drop count for cows/mooshrooms from 0 to 1.
Raised minimum rabbit hide drop count for rabbits from 0 to 1. (Great for making bundles a bit easier to obtain in the early game!)

#### Chest loot

Chest loot tables have been altered such that all entries for the "removed" meats have been merged/replaced with porkchops. The meat versus non-meat item ratios have been kept intact.

Specifics:

-  Replaced potatoes and beetroots in various chests with carrots. The ratio of vegetables to other loot was kept more-or-less the same.
-  Replace beetroot soup in village chests with mushroom stew, which happens to be functionally identical.
-  Removed beetroot seeds from mineshaft, dungeon, spawn bonus, and woodland mansion chests; increased pumpkin/melon seeds to compensate.
-  Replaced beetroot seeds with wheat seeds in village snowy house chests.
-  Replaced beetroot seeds in end city chests with pumpkin seeds, because pumpkins are useful when dealing with endermen.

#### Trading

Butcher villager trades have been adjusted to account for the lack of other meats to buy/sell. All the non-porkchop meat trades are gone (as well as trades involving rabbit stew), and the buy-porkchops trade has been moved from apprentice to journeyman level (because otherwise there wouldn't be anything in that level).

Farmer villager trades involving beetroots, beetroot seeds, and potatoes have been removed.

#### Structures and world generation

Removed potatoes and beetroots from village farms, and increased the number of carrots to compensate in farm biome variants that already generated carrots.

#### Other mechanics

Instead of cats gifting raw chicken to players, they will now gift raw cod. (Changing it to porkchops wouldn't make sense since cats don't hunt pigs.)

For storage convenience, crafting recipes have been added to allow you to convert your existing beef/mutton/etc. into porkchops.

### Java-only changes

The "Hero of the Village" butcher gift loot table has been adjusted to remove non-pork meats. (Bedrock doesn't have the gift mechanic yet, so there's nothing to change there.)

The "removed" meats (+ rabbit stew, for obvious reasons) are no longer required for the "Balanced Diet" challenge advancement.

The removed crops no longer generate in village farms. I'm not sure if it's possible to do this on Bedrock, because I can't find the equivalent of Java Edition's `processor_list` files.

The data pack will alter the trades of novice villagers, but not those that have already leveled up. (I'm actually not sure what the behavior is for the Bedrock pack. I assume that pre-existing villagers are not changed.)

Special thanks to BlockyGoddess for https://www.planetminecraft.com/data-pack/custom-villager-trades-customize-your-villagers-modders-tool/, which was my starting point for the trade-altering code.

## The (Un)licensing

This is free and unencumbered software released into the public domain. See UNLICENSE.txt for more details.
