data remove entity @s Offers.Recipes[-1]
data remove entity @s Offers.Recipes[-1]

execute store result score @s RNG1 run loot spawn ~ -2 ~ loot cv:2

execute if score @s RNG1 matches 1 run data modify entity @s Offers.Recipes append value {buy:{id:"minecraft:wheat",Count:20},sell:{id:"minecraft:emerald",Count:1},xp:2,rewardExp:1b,priceMultiplier:0.05f,maxUses:16}

execute if score @s RNG1 matches 2 run data modify entity @s Offers.Recipes append value {buy:{id:"minecraft:carrot",Count:22},sell:{id:"minecraft:emerald",Count:1},xp:2,rewardExp:1b,priceMultiplier:0.05f,maxUses:16}

data modify entity @s Offers.Recipes append value {buy:{id:"minecraft:emerald",Count:1},sell:{id:"minecraft:bread",Count:6},xp:1,rewardExp:1b,priceMultiplier:0.05f,maxUses:16}

data modify entity @s Xp set value 0
tag @s add check1
function cv:removescores
