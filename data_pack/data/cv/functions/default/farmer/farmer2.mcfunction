data remove entity @s Offers.Recipes[-1]
data remove entity @s Offers.Recipes[-1]

data modify entity @s Offers.Recipes append value {buy:{id:"minecraft:pumpkin",Count:6},sell:{id:"minecraft:emerald",Count:1},xp:10,rewardExp:1b,priceMultiplier:0.05f,maxUses:12}

execute store result score @s RNG1 run loot spawn ~ -2 ~ loot cv:2

execute if score @s RNG1 matches 1 run data modify entity @s Offers.Recipes append value {buy:{id:"minecraft:emerald",Count:1},sell:{id:"minecraft:pumpkin_pie",Count:4},xp:5,rewardExp:1b,priceMultiplier:0.05f,maxUses:12}

execute if score @s RNG1 matches 2 run data modify entity @s Offers.Recipes append value {buy:{id:"minecraft:emerald",Count:1},sell:{id:"minecraft:apple",Count:4},xp:5,rewardExp:1b,priceMultiplier:0.05f,maxUses:16}

tag @s add check2
function cv:removescores
